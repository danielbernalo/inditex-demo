package com.bernal.inditex.worker.prices;

import com.bernal.inditex.worker.prices.jobs.WorkerPricesRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class WorkerPricesApplication {

	@Bean
	public CommandLineRunner applicationRunner() {
		return new WorkerPricesRunner();
	}

	public static void main(String[] args) {
		SpringApplication.run(WorkerPricesApplication.class, args);
	}

}
